# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models

import os
import datetime


def content_file_name(instance, filename):
    """
    Helper function to rename file name
    """
    filename = "{0}_{1}.jpg".format(instance.user.id, instance.user.username)
    return os.path.join('main', 'static', 'img', 'profiles', filename)

"""
User Authentication Models
"""
class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="profile")

    # The additional attributes we wish to include
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to=content_file_name, blank=True)

    def __unicode__(self):
        return self.user.username

    def save_picture(self, *args, **kwargs):
        # delete old file when replacing by updating the file
        try:
            this = UserProfile.objects.get(id=self.id)
            if this.picture != self.picture:
                this.picture.delete(save=False)
        except: pass # when new photo then we do nothing, normal case
        super(UserProfile, self).save(*args, **kwargs)

class RootNotesTb(models.Model):
    note = models.CharField(max_length=10)

class SongsTb(models.Model):
    title = models.CharField(max_length=200)
    writer = models.CharField(max_length=200, blank=True, null=True)
    artist = models.CharField(max_length=200)
    album = models.CharField(max_length=200, blank=True, null=True)
    root_note = models.ForeignKey(RootNotesTb, on_delete=models.DO_NOTHING)
    lyric = models.TextField()
    youtube = models.CharField(max_length=100, blank=True, null=True)
    itunes = models.CharField(max_length=100, blank=True, null=True)
    spotify = models.CharField(max_length=100, blank=True, null=True)
    google_play = models.CharField(max_length=100, blank=True, null=True)
    approval_status = models.CharField(max_length=100)
    is_public = models.CharField(max_length=10, default='true')
    username = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    submit_time = models.DateTimeField(default=datetime.datetime.now())

class TagsTb(models.Model):
    name = models.CharField(max_length=100)
    picture = models.CharField(max_length=100, blank=True, null=True)

class CategoriesTb(models.Model):
    name = models.CharField(unique=True, max_length=200)
    picture = models.CharField(max_length=100, blank=True, null=True)

class PlaylistsTb(models.Model):
    username = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    title = models.CharField(max_length=100, null=True)
    is_public = models.CharField(max_length=10, default='true')
    submit_time = models.DateTimeField(default=datetime.datetime.now())

class CategoriesSongsTb(models.Model):
    category = models.ForeignKey(CategoriesTb, on_delete=models.DO_NOTHING, null=True)
    song = models.ForeignKey(SongsTb, on_delete=models.DO_NOTHING, null=True)

class PlaylistsSongsTb(models.Model):
    playlist = models.ForeignKey(PlaylistsTb, on_delete=models.DO_NOTHING)
    song = models.ForeignKey(SongsTb, on_delete=models.DO_NOTHING, null=True)
    song_index = models.IntegerField(blank=True, null=True)
    root_note = models.ForeignKey(RootNotesTb, on_delete=models.DO_NOTHING, null=True)

class TagsSongsTb(models.Model):
    song = models.ForeignKey(SongsTb, on_delete=models.DO_NOTHING, null=True)
    tag = models.ForeignKey(TagsTb, on_delete=models.DO_NOTHING, null=True)

class ThumbsSongsTb(models.Model):
    username = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    song = models.ForeignKey(SongsTb, on_delete=models.DO_NOTHING, null=True)