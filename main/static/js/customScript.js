// ---- CSRF Token
// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// ----

function updateProfPic(){
	var btn = document.getElementById('profile_picture');
  	btn.click();
}

// convert playlist to a song
function getPlaylistToText(playlist_id){
    $.post("./includes/playlist_text_generator.php",
    {
    	playlist_id: playlist_id
    },
    function(data, status){
		if( status == 'success' ) {
			console.log(data);
			var dl = document.getElementById('get-playlist-text-dl-btn');
  			dl.click();
		}else{
			// can throw error msg here
		}
    });

	return null;
}

// remove delete a playlist
function deletePlaylist(playlist_id){
    $.post("/playlist_updater/",
    {
    	action: 'delete_playlist',
    	playlist_id: playlist_id
    },
    function(data, status){
		if( status == 'success' ) {
			var obj = JSON.parse(data);
			$('#sidebar-playlists-list').html(obj[0]);
			$('#main-container').html('<div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main"><h5>Playlist has been removed</h5></div>');
		}else{
			// can throw error msg here
		}
    });

	return null;
}

// remove a song from a playlist
function removeSongFromPlaylistModal(song_id, playlist_id){
    $.post("/playlist_updater/",
    {
    	action: 'delete_song',
    	playlist_id: playlist_id,
    	song_id: song_id
    },
    function(data, status){
		if( status == 'success' ) {
//			$('#playlists-list').html(data);
			getPlaylistsModal(song_id);
		}else{
			// can throw error msg here
		}
    });

	return null;
}

// add a song to a playlist
function addSongToPlaylistModal(song_id, playlist_id){
	var target_note = document.getElementById('current-note').innerHTML;
	var target_note_id = getCurrentChordId(target_note);
    $.post("/playlist_updater/",
    {
    	action: 'insert_song',
    	playlist_id: playlist_id,
    	song_id: song_id,
    	song_target_note_id: target_note_id
    },
    function(data, status){
		if( status == 'success' ) {
//			$('#playlists-list').html(data);
			getPlaylistsModal(song_id);
		}else{
			// can throw error msg here
		}
    });

	return null;
}

// create a playlist from my playlist page
function createPlaylistFromMyPlaylistPage(){
	var playlist_name = document.getElementById('new_playlist_name').value; // get the playlist name from the field
    $.post("/playlist_updater/",
    {
    	action: 'create_playlist',
    	playlist_name: playlist_name
    },
    function(data, status){
		if( status == 'success' ) {
		    console.log("New playlist has been added successfully ");
		    location.reload();
		}else{
			// can throw error msg here
		}
    });

	return false;
}


// create a playlist
function createPlaylistModal(song_id){
	var playlist_name = document.getElementById('new_playlist_name').value; // get the playlist name from the field
    $.post("/playlist_updater/",
    {
    	action: 'create_playlist',
    	playlist_name: playlist_name
    },
    function(data, status){
		if( status == 'success' ) {
			document.getElementById('new_playlist_name').value = ''; // clear playlist name field
			getPlaylistsModal(song_id);
		}else{
			// can throw error msg here
		}
    });

	return false;
}

// function to get all playlists
function getPlaylistsModal(song_id){

    $.post("/playlist_updater/",
    {
    	action: 'get_playlists',
    	song_id: song_id
    },
    function(data, status){
		if( status == 'success' ) {
			var obj = JSON.parse(data);
			$('#playlists-list').html(obj[0]);
			$('#sidebar-playlists-list').html(obj[1]);

			// // disable fade in
			// var myModalFader = document.getElementsByClassName("modal-backdrop in")[0];
			// myModalFader.style["display"] = "none";

		}else{
			// can throw error msg here
		}
    });


	return null;
}


// function to update a thumb of a song
function updateThumbsCount(song_id) {
    $.post("/thumbs_updater/",
    {
		song_id: song_id
    },
    function(data, status){
		if( status == 'success' ) {
			var obj = JSON.parse(data);
			$('#user-thumbs-count').html(obj[0]);

			// update the thumbs color
			if (obj[1]){
				document.getElementById("user-thumb").style.color = "#A48CD2";
			}else{
				document.getElementById("user-thumb").style.color = "#333";
			}

		}else{
			// can throw error msg here
			// $('#song-lyric-template').html('Error' + status);
		}
    });

}

// // function to update a thumb of a song
// function updateThumbsCount(username, song_id) {
//     $.post("./includes/thumbs_updater.php",
//     {
//     	username: username,
// 		song_id: song_id
//     },
//     function(data, status){
// 		if( status == 'success' ) {
// 			var obj = JSON.parse(data);
// 			$('#user-thumbs-count').html(JSON.stringify(obj['song_thumbs']));

// 			// update the thumbs color
// 			if (obj['user_thumbed'] == true){
// 				document.getElementById("user-thumb").style.color = "#428bca";
// 			}else{
// 				document.getElementById("user-thumb").style.color = "#333";
// 			}

// 		}else{
// 			// can throw error msg here
// 			// $('#song-lyric-template').html('Error' + status);
// 		}
//     });

// }


// function to show playlist box
function postYourAdd () {
    var iframe = $("#forPostyouradd");
    iframe.attr("src", iframe.data("src")); 
}


// handles the click event, sends the query
function getSongInText(base_note, song_title, song_singer) {

	var lyric_temp = document.getElementById('song-lyric-temp').innerHTML;
	var target_note = document.getElementById('current-note').innerHTML;

    $.post("./includes/lyric_generator.php",
    {
		lyric_temp: lyric_temp, 
		base_note: base_note, 
		target_note: target_note,
		song_title: song_title,
		song_singer: song_singer
    },
    function(data, status){
		if( status == 'success' ) {
			// press the link to get the text file
			var dl = document.getElementById('get-text-dl-btn');
  			dl.click();
		}else{
			// can throw error msg here
			// $('#song-lyric-template').html('Error' + status);
		}
    });

	return false;
}

// handles the click event, sends the query
function updateLyricChord(root_note, target_note) {

	var lyric_old = document.getElementById('song-lyric-temp').innerHTML;

    $.post("/lyric_updater/",
    {
		lyric_old: lyric_old,
		root_note: root_note,
		target_note: target_note
    },
    function(data, status){
		if( status == 'success' ) {
			// while(!data);
			$('#song-lyric-template').html(data);
			$('#song-target-note-template').html(target_note);
		}else{
			$('#song-lyric-template').html('Error' + status);
		}
    });

	return false;
}


function changeBaseNote(reset, base_note, direction){
	var chordArray = {
		'C':1,
		'C#':2,
		'D':3,
		'D#':4,
		'E':5,
		'F':6,
		'F#':7,
		'G':8,
		'G#':9,
		'A':10,
		'A#':11,
		'B':12
	};

	// init changeBaseNote.currentNoteIdx variable
	if(changeBaseNote.currentNoteIdx == null || reset == true){
		changeBaseNote.currentNoteIdx = chordArray[base_note]; 
	}

	// increment or decrement counter
	var temp = counter(reset, changeBaseNote.currentNoteIdx, direction);
	changeBaseNote.currentNoteIdx = temp;

	// search for key based on value
	for(var key in chordArray){
		if(chordArray[key] == changeBaseNote.currentNoteIdx){
			break;
		}
	}

	// display current note
	$('#current-note').html(key);

	// update lyric chord
	updateLyricChord(getCurrentChordId(base_note), chordArray[key]);
}

function counter(reset, base_note_idx, direction){
	// initialize counter here if it's null
	if(counter.count == null){
		counter.count = base_note_idx;
	}

	// increment counter based on the direction
	if (reset != true){
		if(direction == 1){
			if (counter.count == 12){
				counter.count = 1;
			}else{
				counter.count++;
			}
		}else if(direction == 0){
			if (counter.count == 1){
				counter.count = 12;
			}else{
				counter.count--;
			}
		}
	}else{
		counter.count = base_note_idx;
	}
	return counter.count;
}

function getCurrentChordId(current_chord){
	var chordArray = {
		'C':1,
		'C#':2,
		'D':3,
		'D#':4,
		'E':5,
		'F':6,
		'F#':7,
		'G':8,
		'G#':9,
		'A':10,
		'A#':11,
		'B':12
	};

	return chordArray[current_chord];
}

