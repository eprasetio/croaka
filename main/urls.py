from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^profile_page/$', views.profile_page, name='profile_page'),
    url(r'^song_page/(?P<song_id>\d+)/$', views.song_page, name='song_page'),
    url(r'^song_page/(?P<song_id>\d+)/(?P<root_note_id>\d+)/$', views.song_page, name='song_page'),
    url(r'^my_song_page/$', views.my_song_page, name='my_song_page'),
    url(r'^favorite_song_page/$', views.favorite_song_page, name='favorite_song_page'),
    url(r'^my_playlist_page/$', views.my_playlist_page, name='my_playlist_page'),
    url(r'^playlist_page/(?P<playlist_id>\d+)/$', views.playlist_page, name='playlist_page'),
    url(r'^message/$', views.message, name='message'),
    url(r'^index/$', views.index, name='index'),
    url(r'^landing_page/$', views.landing_page, name='landing_page'),
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.log_in, name='login'),
    url(r'^logout/$', views.log_out, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^update_profile/$', views.update_profile, name='update_profile'),
    url(r'^search/$', views.search, name='search'),
    url(r'^playlist_updater/$', views.playlist_updater, name='playlist_updater'),
    url(r'^admin_add/$', views.admin_add, name='admin_add'),
    url(r'^admin_update/(?P<song_id>\d+)/$', views.admin_update, name='admin_update'),
    url(r'^thumbs_updater/$', views.thumbs_updater, name='thumbs_updater'),
    url(r'^lyric_updater/$', views.lyric_updater, name='lyric_updater'),
    url(r'^test/$', views.test, name='test')
]