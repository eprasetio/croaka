from django.contrib import admin
from .models import *

# Song Info Model
admin.site.register(CategoriesSongsTb)
admin.site.register(CategoriesTb)
admin.site.register(RootNotesTb)
admin.site.register(PlaylistsSongsTb)
admin.site.register(PlaylistsTb)
admin.site.register(SongsTb)
admin.site.register(TagsSongsTb)
admin.site.register(TagsTb)
admin.site.register(ThumbsSongsTb)

# user authentication model
admin.site.register(UserProfile)