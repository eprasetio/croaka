from django.shortcuts import render, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import *
from django.db.models import Count
from django.core.exceptions import ObjectDoesNotExist

from .models import *
from .forms import UserForm, UserProfileForm
from .includes.songparser import parseSong, getSongHTML, getSongPlainText

import datetime
import json

@login_required
def log_out(request):
    """
    Log out user and redirect to main page.
    :param request:
    :return:
    """
    logout(request)
    return HttpResponseRedirect('/index/')

def log_in(request):
    """
    Login Functionality
    :param request:
    :return:
    """
    context = RequestContext(request) # get the request's context

    # only process the form if it's a HTTP POST
    if request.method == 'POST':
        # grab the user's credentials
        username = request.POST['username']
        password = request.POST['password']

        # use django's authentication system to check if username/password combination is valid
        user = authenticate(username=username, password=password)

        # check if authentication is succesfull by checking the user's object existence
        if user:
            # check if the account is active and not disabled
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/index/')
            else:
                return HttpResponse("Your account is currently get disabled")
        else:
            return HttpResponse("Invalid login details supplied")
    else:
        return render(request, 'login.html', {})

def register(request):
    """
    Process user registration
    :param request:
    :return:
    """
    context = RequestContext(request) # get the request's context

    # flag to indicate whether the registration successful or not
    registered = False

    # only process the form if it's a HTTP POST
    if request.method == 'POST':
        # grab information from the raw data form
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        # continue the process if the two forms are valid
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save() # save user into database

            user.set_password(user.password) # hash the password
            user.save()  # update the user object

            profile = profile_form.save(commit=False) # set commit = False to delay saving the model to database until the user is set
            profile.user = user
            profile.registration_time = datetime.datetime.now()

            # check if the user provide a profile picture
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            # save UserProfile model instance
            profile.save()

            # set the registered flag to True
            registered = True

        # print error message if errors are found in the user_form and profile_form
        else:
            print user_form.errors, profile_form.errors

    # TODO: handle the case if the HTTP request is not POST
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request,
                  'register.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registered': registered})

@login_required
def update_profile(request):
    # only process the form if it's a HTTP POST
    if request.method == 'POST':
        profile = request.user.profile
        print profile.user.username
        print profile.picture

        # check if the user provide a profile picture
        if 'picture' in request.FILES:
            profile.picture = request.FILES['picture']

        profile.save_picture()
    else:
        print "Form submission is not using POST method."

    return redirect('profile_page')

# test view for django
@login_required
def test(request):
    """
    Test View
    :param request:
    :return:
    """
    return render(request, 'test.html', {})

def message(request, content):
    """
    Display a short message to user and redirect user to another page via the provided url
    :param request:
    :return:
    """
    message = 'test'
    dest_url = 'index'

    return render(request, 'message.html', {'message':message, 'url':dest_url})

def landing_page(request):
    """
    Display landing page
    :param request:
    :return:
    """
    return render(request, 'landing_page.html')

def index(request):
    """
    Display main page
    :param request:
    :return:
    """

    if request.user.is_authenticated():
        # get all categories
        categories = CategoriesTb.objects.all()

        # get top songs
        top_songs = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).order_by('-thumbs')[:10]
        top_songs_list = []
        for ts in top_songs:
            if ts.is_public == 'true':
                # get categories for each songs
                print "Song Id:{}".format(ts.id)
                c = CategoriesSongsTb.objects.get(song_id=ts.id)
                print c.category_id
                category = CategoriesTb.objects.get(pk=c.category_id)
                print "Song temp title {}".format(ts.title)
                print "Song temp thumbs: {}".format(ts.thumbs)
                print "Song category {}".format(category.name)
                print "Song username id {}".format(ts.username_id)

                top_songs_list.append({'data':ts, 'category':category.name})

        # get recently added songs
        recent_songs = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).order_by('-submit_time')[:10]
        recent_songs_list = []
        for ts in recent_songs:
            if ts.is_public == "true":
                # get categories for each songs
                print "Song Id:{}".format(ts.id)
                c = CategoriesSongsTb.objects.get(song_id=ts.id)
                print c.category_id
                category = CategoriesTb.objects.get(pk=c.category_id)
                print "Song temp title {}".format(ts.title)
                print "Song temp thumbs: {}".format(ts.thumbs)
                print "Song category {}".format(category.name)
                print "Song username id {}".format(ts.username_id)

                recent_songs_list.append({'data':ts, 'category':category.name})

        # get recently added playlists
        recent_playlists = PlaylistsTb.objects.select_related().order_by('-submit_time')[:10]
        recent_playlists_list = []
        for ts in recent_playlists:
            if ts.is_public == "true":
                recent_playlists_list.append({'data':ts})

        # get playlists for side bar
        if request.user.is_authenticated():
            playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)
        else:
            playlists = None

        return render(request, 'index.html',
            {
                'categories': categories,
                'top_songs_list':top_songs_list,
                'recent_songs_list':recent_songs_list,
                'recent_playlists_list': recent_playlists_list,
                'playlists':playlists
            }
        )
    else:
        return render(request, 'landing_page.html')

def song_page(request, song_id, root_note_id=None):
    song = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).get(id=song_id)
    c = CategoriesSongsTb.objects.get(song_id=song.id)
    category = CategoriesTb.objects.get(pk=c.category_id)
    if root_note_id:
        lyric = getSongHTML(lyric_json=song.lyric, base=root_note_id, transpose=song.root_note_id)
    else:
        lyric = getSongHTML(lyric_json=song.lyric, base=song.root_note_id, transpose=song.root_note_id)

    if request.user.is_authenticated():
        # get playlists for side bar
        playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)
        # check if user has liked the song
        is_thumbed = ThumbsSongsTb.objects.filter(song_id=song_id, username_id=request.user.profile.user.id).exists()
    else:
        playlists = None
        is_thumbed = None

    # check if the user already thumbed the song

    return render(request, 'song_page.html', {"song":song,
                                              "root_node_id":root_note_id,
                                              "lyric":lyric,
                                              "category": category,
                                              "playlists":playlists,
                                              "is_thumbed":is_thumbed})

def search(request):
    song_search_results = []
    playlist_search_results = []
    if request.method == 'GET':
        search_keywords = request.GET['search_keywords']
        search_results = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).filter(title__icontains=str(search_keywords))
        print search_results
        for song in search_results:
            if song.is_public == 'true':
                # get categories for each songs
                c = CategoriesSongsTb.objects.get(song_id=song.id)
                category = CategoriesTb.objects.get(pk=c.category_id)
                song_search_results.append({'data':song, 'category':category})

        search_results = PlaylistsTb.objects.select_related().filter(title__icontains=str(search_keywords))
        print search_results
        for playlist in search_results:
            if playlist.is_public == 'true':
                playlist_search_results.append({'data':playlist})
    else:
        print "Form submission is not using GET method."

    # get playlists for side bar
    if request.user.is_authenticated():
        playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)
    else:
        playlists = None

    return render(request, 'search.html', {"song_search_results": song_search_results,
                                           "playlist_search_results": playlist_search_results,
                                           "playlists":playlists})

@login_required
def profile_page(request):
    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    return render(request, 'profile_page.html', {"playlists":playlists})

@login_required
def my_playlist_page(request):
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)
    return render(request, 'my_playlist_page.html', {"playlists": playlists})

@login_required
def playlist_page(request, playlist_id):
    songs = []
    playlist = PlaylistsTb.objects.get(pk=playlist_id)
    pl_songs = PlaylistsSongsTb.objects.filter(playlist_id=playlist_id).order_by('song_index')
    for pl_song in pl_songs:
        data = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).get(pk=pl_song.song_id)
        if data.is_public == 'true':
            c = CategoriesSongsTb.objects.get(song_id=pl_song.song_id)
            category = CategoriesTb.objects.get(pk=c.category_id)
            root_note = RootNotesTb.objects.get(pk=pl_song.root_note_id)
            songs.append({"data":data,
                          "category":category,
                          "root_note":root_note,
                          "index":pl_song.song_index})

    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    return render(request, 'playlist_page.html', {"songs": songs,
                                                  "playlist": playlist,
                                                  "playlists":playlists})

@login_required
def my_song_page(request):
    songs = []
    my_songs = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).filter(username=request.user.profile.user.id)

    for s in my_songs:
        # get categories for each songs
        print s.id
        c = CategoriesSongsTb.objects.get(song_id=s.id)
        category = CategoriesTb.objects.get(pk=c.category_id)
        songs.append({'data':s, 'category':category})

    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    return render(request, 'my_song_page.html', {"songs":songs,
                                                 "playlists":playlists})

@login_required
def favorite_song_page(request):
    songs = []
    thumbed_songs = ThumbsSongsTb.objects.filter(username_id=request.user.profile.user.id)

    for ts in thumbed_songs:
            # get the thumbed song data
            s = SongsTb.objects.select_related().annotate(thumbs=Count('thumbssongstb')).get(pk=ts.song_id)

            if s.is_public == 'true':
                # get categories for each songs
                c = CategoriesSongsTb.objects.get(song_id=s.id)
                category = CategoriesTb.objects.get(pk=c.category_id)
                songs.append({'data':s, 'category':category})

    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    return render(request, 'favorite_song_page.html', {"songs":songs,
                                                       "playlists":playlists})

@login_required
def admin_add(request):
    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    if request.method == 'POST':
        print "add new song..."
        new_song = SongsTb.objects.create(
            title=request.POST['title'],
            artist = request.POST['artist'],
            writer = request.POST['writer'],
            album = request.POST['album'],
            root_note = RootNotesTb.objects.get(id=request.POST['root_note_id']),
            lyric = json.dumps(parseSong(request.POST['lyric'])),
            youtube = request.POST['youtube'],
            itunes = request.POST['itunes'],
            spotify = request.POST['spotify'],
            google_play = request.POST['google_play'],
            approval_status = "waiting",
            is_public = request.POST['is_public'],
            username = request.user,
            submit_time = datetime.datetime.now()
        )
        new_song.save()    # save the new song object in the database

        # save the song's category
        new_category_song = CategoriesSongsTb.objects.create(
            category=CategoriesTb.objects.get(id=request.POST['category_id']),
            song=new_song
        )
        new_category_song.save()

        # add message and dest url after adding new data
        url = 'message.html'
        content = {'message':'Successfully added {}'.format(request.POST['title']),
                   'dest_url':'my_song_page',
                   'playlists':playlists}

        # TODO: add tag later
    else:
        print "just load the page"
        url = 'admin_add.html'
        content = {"playlists":playlists}

    return render(request, url, content)

@login_required
def admin_update(request, song_id):
    # TODO: Need to alter the table content, not creating a new one

    song = SongsTb.objects.get(pk=song_id)
    category_song = CategoriesSongsTb.objects.get(song_id=song_id)

    # get playlists for side bar
    playlists = PlaylistsTb.objects.filter(username_id=request.user.profile.user.id)

    if request.method == 'POST':
        if request.POST['action'] == 'update':
            print "Updating song..."
            song = SongsTb.objects.get(pk=song_id)
            category_song = CategoriesSongsTb.objects.get(song_id=song_id)

            # update category song
            category_song.category_id = request.POST['category_id']

            # updating song fields
            song.title=request.POST['title']
            song.artist = request.POST['artist']
            song.writer = request.POST['writer']
            song.album = request.POST['album']
            song.root_note = RootNotesTb.objects.get(id=request.POST['root_note_id'])
            song.lyric = json.dumps(parseSong(request.POST['lyric']))
            song.youtube = request.POST['youtube']
            song.itunes = request.POST['itunes']
            song.spotify = request.POST['spotify']
            song.google_play = request.POST['google_play']
            song.approval_status = "waiting"
            song.is_public = request.POST['is_public']
            song.username = request.user
            song.submit_time = datetime.datetime.now()

            song.save()    # save the new song object in the database

            # add message and dest url after adding new data
            url = 'message.html'
            content = {'message':'Successfully updated {}'.format(request.POST['title']),
                   'dest_url':'my_song_page',
                    'playlists':playlists}

        elif request.POST['action'] == 'delete':
            print "Deleting song..."
            try:
                song = SongsTb.objects.get(pk=song_id)
                song.delete()
                print "deleting song from song Tb"
            except ObjectDoesNotExist:
                pass

            try:
                category_song = CategoriesSongsTb.objects.get(song_id=song_id)
                category_song.delete()
                print "deleting song from category song Tb"
            except ObjectDoesNotExist:
                pass

            try:
                playlist_song = PlaylistsSongsTb.objects.get(song_id=song_id)
                playlist_song.delete()
                print "deleting song from Playlist song Tb"
            except:
                pass

            try:
                tag_song = TagsSongsTb.objects.get(song_id=song_id)
                tag_song.delete()
                print "deleting song from tag song Tb"
            except:
                pass

            try:
                thumb_song = ThumbsSongsTb.objects.get(song_id=song_id)
                thumb_song.delete()
                print "deleting song from Thumb song Tb"
            except:
                pass

            url = 'message.html'
            content = {'message':'Successfully deleted song',
                       'dest_url':'my_song_page',
                       'playlists':playlists}

        # TODO: add tag later
    else:
        print "just load the update page"
        url = 'admin_update.html'
        content = {"song":song,
                   "lyric_plain_text":getSongPlainText(song.lyric, song.root_note.id, song.root_note.id),
                   "category":category_song,
                   "playlists":playlists}

    return render(request, url, content)

@login_required
def playlist_updater(request):
    print "Entering Playlist Updater"
    html_str = []

    if request.method == 'POST':
        if request.POST['action'] == 'delete_playlist':
            print "deleting playlist"
            playlist_id = request.POST['playlist_id']
            # delete all song-playlist relation data
            target_songs = PlaylistsSongsTb.objects.filter(playlist_id=playlist_id)
            for song in target_songs:
                song.delete()
            # delete playlist data
            target_playlist = PlaylistsTb.objects.get(pk=playlist_id)
            target_playlist.delete()
            # get the updated playlists list for the side bar
            playlist_modal_str, sidebar_playlist_str = get_playlist_modal_list(user=request.user.profile.user.id)
            html_str.append(sidebar_playlist_str)
        elif request.POST['action'] == 'delete_song':
             print "delete song"
             playlist_id = request.POST['playlist_id']
     	     song_id = request.POST['song_id']
             print playlist_id
             print song_id
             target_song = PlaylistsSongsTb.objects.get(song_id=song_id, playlist_id=playlist_id)
             target_song.delete()
        elif request.POST['action'] == 'insert_song':
            print "insert_song"
            playlist_id = request.POST['playlist_id']
            song_id = request.POST['song_id']
            song_target_note_id = request.POST['song_target_note_id']

            # get number of songs in the target playlist
            total_songs = len(PlaylistsSongsTb.objects.filter(playlist_id=playlist_id))
            new_song_playlist_record = PlaylistsSongsTb.objects.create(
                playlist_id = playlist_id,
                song_id = song_id,
                root_note_id = song_target_note_id,
                song_index = total_songs+1
            )
            new_song_playlist_record.save()
        elif request.POST['action'] == 'create_playlist':
            playlist_name = request.POST['playlist_name']
            new_playlist_record = PlaylistsTb.objects.create(username=request.user, title=playlist_name, submit_time=datetime.datetime.now())
            new_playlist_record.save()
            print "create_playlist"
        elif request.POST['action'] == 'get_playlists':
            print "get_playlists"
            song_id = request.POST['song_id']

            playlist_modal_str, sidebar_playlist_str = get_playlist_modal_list(request.user.profile.user.id, song_id)
            html_str.append(playlist_modal_str)
            html_str.append(sidebar_playlist_str)
        elif request.POST['action'] == 'set_playlist_public':
            print "setting playlist as {}".format(request.POST['is_public'])
            modified_playlist = PlaylistsTb.objects.get(pk=request.POST['playlist_id'])
            modified_playlist.is_public = request.POST['is_public']
            modified_playlist.save()

    return HttpResponse(json.dumps(html_str))

def get_playlist_modal_list(user, song_id=None):
    playlist_modal_str = ""
    sidebar_playlist_str = ""

    playlist = PlaylistsTb.objects.filter(username_id=user)

    for p in playlist:
        if song_id:
            song_exist = PlaylistsSongsTb.objects.filter(playlist_id=p.id, song_id=song_id).exists()
            if song_exist:
                playlist_modal_str += "<p class='playlist-name'><span onclick='removeSongFromPlaylistModal({0}, {1})' \
                    class='glyphicon glyphicon-check' aria-hidden='true'></span>{2}</p>".format(
                    song_id,
                    p.id,
                    p.title
                )
            else:
                playlist_modal_str += "<p class='playlist-name'><span onclick='addSongToPlaylistModal({0}, {1})' \
                    class='glyphicon glyphicon-unchecked' aria-hidden='true'></span>{2}</p>".format(
                    song_id,
                    p.id,
                    p.title
                )

        sidebar_playlist_str += "<li><a href='/{0}/{1}'> \
            <span class='sidebar-icon glyphicon glyphicon-list' aria-hidden='true'> \
            </span>{2}</a></li>".format('playlist_page', p.id, p.title)

    return playlist_modal_str, sidebar_playlist_str


@login_required
def lyric_updater(request):
    print "Entering Lyric Updater"
    lyric = None

    if request.method == 'POST':
        lyric_old = request.POST['lyric_old']
        root_note = request.POST['root_note']
        target_note = request.POST['target_note']

        print lyric_old
        print root_note
        print target_note
        lyric = getSongHTML(lyric_json=lyric_old, base=int(root_note), transpose=int(target_note))

    return HttpResponse(lyric)

@login_required
def thumbs_updater(request):
    print "Entering Lyric Updater"
    html_str = []

    if request.method == 'POST':
        song_id = request.POST['song_id']

        thumb = ThumbsSongsTb.objects.filter(song_id=song_id, username_id=request.user.profile.user.id)
        if thumb.exists():
            thumb.delete()
            is_thumbed = False
        else:
            new_thumb = ThumbsSongsTb.objects.create(song_id=song_id, username_id=request.user.profile.user.id)
            new_thumb.save()
            is_thumbed = True

    # get number of thumbs for the song
    number_of_thumbs = len(ThumbsSongsTb.objects.filter(song_id=song_id))

    html_str.append(number_of_thumbs)
    html_str.append(is_thumbed)

    return HttpResponse(json.dumps(html_str))