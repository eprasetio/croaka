from .models import UserProfile
from django.contrib.auth.models import User
from django import forms

import datetime

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    last_login = datetime.datetime.now()

    class Meta:
        model = User
        fields = ('username',
                  'email',
                  'password',
                  'first_name',
                  'last_name')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website', 'picture')