from django.utils.safestring import mark_safe
from django.utils.encoding import force_text

import json

# TODO: Get original text input

def getSongPlainText(lyric_json, base=7, transpose=7):
    """
    Display the song + chords into a plain text from the JSON format.
    :param base:
    :param transpose:
    :return:
    """
    print "Trans:{}",transpose
    print "Base:{}",base
    transpose = int(transpose)
    base = int(base)
    if transpose<base:
        offset = abs(transpose+12-base)
    else:
        offset = abs(transpose-base)
    string = ""

    print lyric_json
    print "============="
    song = json.loads(lyric_json)
    print song

    for s in song:
        print s
        if s["type"] == "title":
            string += ":t: "
            string += s["content"]
        elif s["type"] == "intro":
            string += ":i: "
            for c in s["content"]["chords"]:
                string += getChordFromIndex(c["chord"], offset)+c["extra"]
                string += " "
            string += s["content"]["repeat"]
        elif s["type"] == "chord":
            string += ":c: "
            prev_idx = 0
            chord_str = ""
            for c in s["content"]:
                # print the spaces
                print c
                idx = c["index"] - prev_idx - len(":chord:")-1
                print "Prev Idx: {}".format(prev_idx)
                print "Idx: {}".format(idx)
                for i in range(idx):
                    chord_str += " "
                chord = getChordFromIndex(c["chord"], offset)+c["extra"]
                chord_str += chord
                print "Chord_Str: {}".format(chord_str)
                prev_idx = len(chord)+idx+prev_idx

            string += chord_str
        elif s["type"] == "lyric":
            string += ":l: "
            string += s["content"]
        elif s["type"] == "comment":
            string += "# "
            string += s["content"]
        elif s["type"] == "empty":
            string += "" # print empty string

        string += "\n"

    print string
    return force_text(string)

def getSongHTML(lyric_json, base=7, transpose=7):
    """
    Display the song + chords from the JSON format.
    :param base:
    :param transpose:
    :return:
    """
    print "Trans:{}",transpose
    print "Base:{}",base
    transpose = int(transpose)
    base = int(base)
    if transpose<base:
        offset = abs(transpose+12-base)
    else:
        offset = abs(transpose-base)
    string = ""

    print lyric_json
    print "============="
    song = json.loads(lyric_json)
    print song

    for s in song:
        print s
        if s["type"] == "intro":
            string += "<div class='text-seg'>Intro:&nbsp;</div>"
            string += "<div class='chord-line'>"
            for c in s["content"]["chords"]:
                string += getChordFromIndex(c["chord"], offset)+c["extra"]
                string += "&nbsp;"
            string += s["content"]["repeat"]
            string += "</div><br>"
        elif s["type"] == "chord":
            string += "<div class='chord-line'>"
            prev_idx = 0
            chord_str = ""
            for c in s["content"]:
                # print the spaces
                print c
                idx = c["index"] - prev_idx - len(":chord:")-1
                print "Prev Idx: {}".format(prev_idx)
                print "Idx: {}".format(idx)
                for i in range(idx):
                    chord_str += "&nbsp;"
                chord = getChordFromIndex(c["chord"], offset)+c["extra"]
                chord_str += chord
                print "Chord_Str: {}".format(chord_str)
                prev_idx = len(chord)+idx+prev_idx

            string += chord_str
            string += "</div>"
        elif s["type"] == "lyric":
            string += "<div class='lyric-line'>"
            string += s["content"]
            string += "</div>"
        elif s["type"] == "comment":
            string += "<div class='tex-seg'>"
            string += s["content"]
            string += "</div>"
        elif s["type"] == "empty":
            string += "<br>"

        string += "\n"

    print string
    return mark_safe(string)

def parseSong(f):
    """
    Parse a string of lyrics + chord into a JSON representative format
    :param path_to_song:
    :return:
    """
    song_json = []

    # # read the song text file and split each lines into a list
    # with open(path_to_song, 'r') as f:
    # lines = list(f)
    lines = f.split("\n")
    print lines

    # iterate every lines and parse them
    for line in lines:
        # split each words in the line
        l = line.split()
        temp = {}

        # check if the line has a special character (:) in the beginning
        if l and ":" == l[0][0]:
            # found title line
            if ":title:" in l[0] or ":t:" in l[0]:
                temp = {
                        "type": "title",
                        "content": " ".join(l[1::])
                    }
            # found intro line
            elif ":intro:" in l[0] or ":i:" in l[0]:
                chords = []
                temp = {
                        "type": "intro",
                        "content": {
                                "chords":[],
                                "repeat":l[-1]
                        }
                    }
                for c in l[1:-1]:
                    # findStrIndex(line, c)
                    chord_info = {}
                    chord_info.update(parseChord(c))
                    chords.append(chord_info)

                temp["content"]["chords"] = chords
            # found chord lines
            elif ":chord:" in l[0] or ":c:" in l[0]:

                # grab all the chord and its index
                chords = []
                for c in l[1::]:
                    # findStrIndex(line, c)
                    chord_info = {"index":line.find(c)}
                    chord_info.update(parseChord(c))
                    chords.append(chord_info)
                temp = {
                        "type": "chord",
                        "content": chords
                    }
            # found lyric lines
            elif ":lyric:" in l[0] or ":l:" in l[0]:
                temp = {
                        "type": "lyric",
                        "content": " ".join(l[1::])
                    }
            else:
                temp = {
                    "type": "empty",
                    "content": None
            }
        # check if a comment line is found
        elif l and "#" == l[0][0]:
            temp = {
                    "type": "comment",
                    "content": l[0][1::]
                }
        else:
            temp = {
                    "type": "empty",
                    "content": None
            }

        #print "temp: {}".format(temp)
        song_json.append(temp)

    return song_json

def parseChord(chord_str):
    """
    Separate the base chord character and its additional characters.
    :param chord_str:
    :return:
    """
    if "#" in chord_str:
        return {
                "chord":getIndexFromChord(chord_str[:2]),
                "extra":chord_str[2:]
                }
    else:
        return {
                "chord":getIndexFromChord(chord_str[:1]),
                "extra":chord_str[1:]
                }

def getIndexFromChord(chord):
    """
    Get the integer index value that represents the input chord
    :param chord:
    :return:
    """
    chords = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    return chords.index(chord)

def getChordFromIndex(index, offset=0):
    """
    Get the chord string value based on the input index.
    :param index:
    :param offset:
    :return:
    """
    chords = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    return chords[(index+offset)%12]